package com.example.dmotpan.firstproject.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dmotpan.firstproject.DatabaseHelper;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.activities.ListViewActivity;
import com.example.dmotpan.firstproject.adapters.PersonAdapter;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by dmotpan on 7/12/16.
 */
public abstract class BaseFragment extends Fragment {
    protected PersonAdapter personAdapter;
    protected ArrayList<Person> auxList = DatabaseHelper.getInstance(getContext()).getAllPersons();


    public abstract void addPerson(Person person);
    public void addPersonFromDb() {
        int num = new Random().nextInt(6);
        personAdapter.addItem(auxList.get(num));
        personAdapter.notifyDataSetChanged();
    }

}
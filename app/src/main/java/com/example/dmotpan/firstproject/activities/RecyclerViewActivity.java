package com.example.dmotpan.firstproject.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;
import com.example.dmotpan.firstproject.adapters.RecyclerAdapter;

import java.util.Random;

public class RecyclerViewActivity extends BaseActivity {
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addPerson:
                recyclerAdapter.addItem(new Person(Names.randomChoosePerson(), imageView));
                break;
            case R.id.addPersonDb:
                Person person = databaseHelper.getPerson(new Random().nextInt(6) + 1);
                setPersonName(person);
                getPersonPhoto(person);
                person.setPhoto(imageView);
                recyclerAdapter.addItem(person);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setPersonName(Person person) {
        TextView textView = new TextView(this);
        textView.setText(person.getName());
    }

    private void getPersonPhoto(Person person) {
        Bitmap bitmap = BitmapFactory
                .decodeByteArray(person.getByteImage(), 0, person.getByteImage().length);
        imageView = new ImageView(this);
        imageView.setImageBitmap(bitmap);
        imageView.requestLayout();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recicler_view);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerAdapter = new RecyclerAdapter(list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);
    }
}

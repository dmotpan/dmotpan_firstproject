package com.example.dmotpan.firstproject.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.dmotpan.firstproject.DatabaseHelper;
import com.example.dmotpan.firstproject.adapters.PersonAdapter;
import com.example.dmotpan.firstproject.R;

public class ListViewActivity extends BaseActivity {

    DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addPerson:
                addPersonToAdapter();
                break;
            case R.id.addPersonDb:
                addFromDatabase();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView = (ListView) findViewById(R.id.listView);
        list = databaseHelper.getAllPersons();
        adapter = new PersonAdapter(this);
        listView.setAdapter(adapter);
    }
}

package com.example.dmotpan.firstproject.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;

import java.util.ArrayList;

/**
 * Created by dmotpan on 7/5/16.
 */
public class PersonAdapter extends BaseAdapter {

    Person person;
    Activity activity;

    ArrayList <Person> persons = new ArrayList<>();

    public PersonAdapter(Activity activity) {
        this.activity = activity;
    }

    public PersonAdapter(Activity activity, ArrayList<Person> persons) {
        this.activity = activity;
        this.persons = persons;
    }

    private static class ViewHolder {
        TextView name;
        ImageView photo;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void addItem(Person person) {
        persons.add(person);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        person = persons.get(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.firstLastNameTextView);
            viewHolder.photo = (ImageView) convertView.findViewById(R.id.imageViewPhoto);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(person.name);
        viewHolder.photo.setImageResource(setPhoto());

        Button buttonDelete = (Button) convertView.findViewById(R.id.deleteButton);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                persons.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public int setPhoto() {
        switch (person.getName()) {
            case Names.ARCADII:
                return R.drawable.arubailo;
            case Names.DMITRII:
                return R.drawable.motpan;
            case Names.FILIP:
                return R.drawable.filip;
            case Names.VADIM:
                return R.drawable.vadimm;
            case Names.SASHA_BULAT:
                return R.drawable.sashab;
            case Names.SASHA_BOND:
                return R.drawable.sashabog;
            default:
                return R.drawable.motpan;
        }
    }
}

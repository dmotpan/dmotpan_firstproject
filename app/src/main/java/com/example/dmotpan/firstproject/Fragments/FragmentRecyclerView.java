package com.example.dmotpan.firstproject.fragments;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;
import com.example.dmotpan.firstproject.adapters.RecyclerAdapter;

public class FragmentRecyclerView extends BaseFragment {

    RecyclerAdapter adapter;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager mLayoutManager;

    public FragmentRecyclerView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_recicler_view, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);
        configureAdapter();
        return rootView;
    }

    private void configureAdapter() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new RecyclerAdapter(auxList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addPerson(Person person) {
        auxList.add(person);
        adapter.notifyDataSetChanged();
    }
}

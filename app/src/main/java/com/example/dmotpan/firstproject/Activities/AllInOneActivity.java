package com.example.dmotpan.firstproject.activities;

import android.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.dmotpan.firstproject.fragments.BaseFragment;
import com.example.dmotpan.firstproject.adapters.MyPagerAdapter;
import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;
import com.example.dmotpan.firstproject.fragments.FragmentRecyclerView;
import com.example.dmotpan.firstproject.fragments.FragmentTableLayout;

public class AllInOneActivity extends BaseActivity {

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView navigationDrawer;
    ViewPager viewPager;
    int currentPosition;
    MyPagerAdapter pagerAdapter;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_in_one);

        findAllViewsById();

        setSupportActionBar(toolbar);
        setupDrawerContent(navigationDrawer);
        configureTabLayout();
        setTabListener();

        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void findAllViewsById() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationDrawer = (NavigationView) findViewById(R.id.nvView);
        viewPager = (ViewPager) findViewById(R.id.vPager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    private void setTabListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        navigationDrawer.setCheckedItem(R.id.menuListView);
                        setTitle("ListView");
                        break;
                    case 1:
                        navigationDrawer.setCheckedItem(R.id.menuGridView);
                        setTitle("GridView");
                        break;
                    case 2:
                        navigationDrawer.setCheckedItem(R.id.menuTableLayout);
                        setTitle("TableLayout");
                        break;
                    case 3:
                        navigationDrawer.setCheckedItem(R.id.menuRecyclerView);
                        setTitle("RecyclerView");
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}
            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    private void configureTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("ListView"));
        tabLayout.addTab(tabLayout.newTab().setText("GridView"));
        tabLayout.addTab(tabLayout.newTab().setText("Table"));
        tabLayout.addTab(tabLayout.newTab().setText("Recycler"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.menuListView:
                currentPosition = 0;
                viewPager.setCurrentItem(currentPosition);
                break;
            case R.id.menuGridView:
                currentPosition = 1;
                viewPager.setCurrentItem(currentPosition);
                break;
            case R.id.menuTableLayout:
                currentPosition = 2;
                viewPager.setCurrentItem(currentPosition);
                break;
            case R.id.menuRecyclerView:
                currentPosition = 3;
                viewPager.setCurrentItem(currentPosition);
                break;
        }

        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String name = Names.randomChoosePerson();
        BaseFragment fragment =
                (BaseFragment)pagerAdapter.getRegisteredFragment(viewPager.getCurrentItem());
        switch (item.getItemId()) {
            case R.id.addPerson:
                fragment.addPerson(new Person(name, setPhoto(name)));
                break;
            case R.id.addPersonDb:
                if (fragment instanceof FragmentRecyclerView
                        || fragment instanceof FragmentTableLayout) {
                    fragment.addPerson(new Person(name, setPhoto(name)));
                } else {
                    ((BaseFragment) (pagerAdapter.getRegisteredFragment(viewPager.getCurrentItem())))
                            .addPersonFromDb();
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_item, menu);
        return true;
    }

    private ImageView setPhoto(String str) {
        ImageView imageView = new ImageView(this);
        switch (str) {
            case Names.DMITRII:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.motpan));
                break;
            case Names.FILIP:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.filip));
                break;
            case Names.ARCADII:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.arubailo));
                break;
            case Names.SASHA_BULAT:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.sashab));
                break;
            case Names.VADIM:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.vadimm));
                break;
            case Names.SASHA_BOND:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.sashabog));
                break;
        }
        return imageView;
    }
}
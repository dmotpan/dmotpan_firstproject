package com.example.dmotpan.firstproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmotpan on 7/6/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance;

    private static final String DATABASE_NAME = "personsDatabase";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_PERSONS = "persons";

    public static final String PERSON_ID = "ID";
    public static final String PERSON_NAME = "name";
    public static final String PERSON_PHOTO = "photo";


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_PERSONS +
                "(" + PERSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        PERSON_NAME + " TEXT, " +
                        PERSON_PHOTO + " MEDIUMBLOB" +
                ")";
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_PERSONS);
            onCreate(sqLiteDatabase);
        }
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if(sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public void addPerson(Person person) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PERSON_NAME, person.name);
        contentValues.put(PERSON_PHOTO, person.byteImage);
        sqLiteDatabase.insert(TABLE_PERSONS, null, contentValues);

    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    public Person getPerson(int id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(TABLE_PERSONS, new String[] {
                PERSON_ID, PERSON_NAME, PERSON_PHOTO},
                PERSON_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
            Person person = new Person(cursor.getString(cursor.getColumnIndex(PERSON_NAME)),
                    cursor.getBlob(cursor.getColumnIndex(PERSON_PHOTO)));
            return person;
    }

    public ArrayList<Person> getAllPersons() {
        ArrayList <Person> list = new ArrayList<Person>();
        String SELECT_QUERY = "SELECT * FROM " + TABLE_PERSONS;
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    Person person = new Person(cursor.getString(cursor.getColumnIndex(PERSON_NAME)),
                            cursor.getBlob(cursor.getColumnIndex(PERSON_PHOTO)));
                    list.add(person);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("ERROR", "failed to select from databse");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
            return list;
    }

    public boolean isEmpty() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_PERSONS;
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            return false;
        }
        return true;
    }
}
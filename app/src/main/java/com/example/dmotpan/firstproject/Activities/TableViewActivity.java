package com.example.dmotpan.firstproject.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;

import java.util.Random;

public class TableViewActivity extends BaseActivity {

    TableLayout tableLayout;
    ImageView imageView;
    TableRow tableRow;
    String name;
    View view;
    TextView textView;
    Button deleteButton;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        initUi();
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRow(view);
            }
        });
        switch (item.getItemId()) {
            case R.id.addPerson:
                name = Names.randomChoosePerson();
                setPhoto();
                textView.setText(name);
                break;
            case R.id.addPersonDb:
                Person person = databaseHelper.getPerson(new Random().nextInt(6) + 1);
                textView.setText(person.getName());
                setPersonPhoto(person);
        }
        tableLayout.addView(tableRow);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_view);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
    }

    private void initUi() {
        tableRow = new TableRow(this);
        view = getLayoutInflater().inflate(R.layout.list_item, tableRow);
        textView = (TextView) view.findViewById(R.id.firstLastNameTextView);
        deleteButton = (Button) view.findViewById(R.id.deleteButton);
        imageView = (ImageView) view.findViewById(R.id.imageViewPhoto);
    }

    private void deleteRow(View view) {
        View row = (View) view.getParent();
        ViewGroup container = ((ViewGroup) row.getParent());
        container.removeView(row);
        container.invalidate();
    }

    private void setPersonPhoto(Person person) {
        Bitmap bitmap = BitmapFactory
                .decodeByteArray(person.getByteImage(), 0, person.getByteImage().length);
        imageView.setImageBitmap(bitmap);
        imageView.requestLayout();
        person.setPhoto(imageView);
    }

    private void setPhoto() {
        switch (name) {
            case Names.DMITRII:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.motpan));
                break;
            case Names.FILIP:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.filip));
                break;
            case Names.ARCADII:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.arubailo));
                break;
            case Names.SASHA_BULAT:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.sashab));
                break;
            case Names.VADIM:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.vadimm));
                break;
            case Names.SASHA_BOND:
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.sashabog));
                break;
        }
    }
}

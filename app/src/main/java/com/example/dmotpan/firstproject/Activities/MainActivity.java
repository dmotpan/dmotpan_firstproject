package com.example.dmotpan.firstproject.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.dmotpan.firstproject.DatabaseHelper;
import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;

public class MainActivity extends AppCompatActivity {

    Button buttonListview, buttonGridview, buttonTableview, buttonRecyclerView, buttonAllInOne;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHelper = DatabaseHelper.getInstance(this);
        initUi();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setIntents(view);
            }
        };
        buttonListview.setOnClickListener(onClickListener);
        buttonGridview.setOnClickListener(onClickListener);
        buttonTableview.setOnClickListener(onClickListener);
        buttonRecyclerView.setOnClickListener(onClickListener);
        buttonAllInOne.setOnClickListener(onClickListener);

        if (databaseHelper.isEmpty()) {
            for (int i = 0; i < decodeImages().length; i++) {
                databaseHelper.addPerson(new Person
                        (Names.NAMES[i], DatabaseHelper.getBytes(decodeImages()[i])));
            }
        }
    }


    private void initUi() {
        buttonListview = (Button) findViewById(R.id.buttonListView);
        buttonGridview = (Button) findViewById(R.id.buttonGridView);
        buttonTableview = (Button) findViewById(R.id.buttonTableView);
        buttonRecyclerView = (Button) findViewById(R.id.buttonRecycleView);
        buttonAllInOne = (Button) findViewById(R.id.buttonAllInOne);
    }

    private void setIntents(View view) {
        switch (view.getId()) {
            case R.id.buttonListView:
                start(ListViewActivity.class);
                break;
            case R.id.buttonGridView:
                start(GridViewActivity.class);
                break;
            case R.id.buttonTableView:
                start(TableViewActivity.class);
                break;
            case R.id.buttonRecycleView:
                start(RecyclerViewActivity.class);
                break;
            case R.id.buttonAllInOne:
                start(AllInOneActivity.class);
                break;
        }
    }

    private void start(Class activity) {
        startActivity(new Intent(MainActivity.this, activity));
    }

    public Bitmap decodeImage(int id) {
        Bitmap image = BitmapFactory.decodeResource(getResources(), id);
        return image;
    }
    public Bitmap [] decodeImages() {
        Bitmap [] bitmapArray = new Bitmap[6];

        bitmapArray[0] = decodeImage(R.drawable.motpan);
        bitmapArray[1] = decodeImage(R.drawable.arubailo);
        bitmapArray[2] = decodeImage(R.drawable.filip);
        bitmapArray[3] = decodeImage(R.drawable.sashab);
        bitmapArray[4] = decodeImage(R.drawable.vadimm);
        bitmapArray[5] = decodeImage(R.drawable.sashabog);

        return  bitmapArray;
    }
}

package com.example.dmotpan.firstproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.adapters.PersonAdapter;
import com.example.dmotpan.firstproject.R;

public class FragmentListView extends BaseFragment {

    ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_view, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        personAdapter = new PersonAdapter(getActivity());
        listView.setAdapter(personAdapter);
        return rootView;
    }

    @Override
    public void addPerson(Person person) {
        personAdapter.addItem(person);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}


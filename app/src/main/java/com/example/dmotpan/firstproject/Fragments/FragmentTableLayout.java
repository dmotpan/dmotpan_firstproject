package com.example.dmotpan.firstproject.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FragmentTableLayout extends BaseFragment {
    TableRow tableRow;
    TableLayout tableLayout;
    TextView textView;
    ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_table_layout, container, false);
        tableLayout = (TableLayout) rootView.findViewById(R.id.tableLayout);
        return rootView;
    }

    public void addPerson(Person person) {
        initUi();
        textView.setText((person.getName()));
        imageView.setImageDrawable(person.getPhoto().getDrawable());
        tableLayout.addView(tableRow);
    }

    private void initUi() {
        tableRow = new TableRow(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.list_item, tableRow);

        textView = (TextView) view.findViewById(R.id.firstLastNameTextView);
        imageView = (ImageView) view.findViewById(R.id.imageViewPhoto);

        view.findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View row = (View) view.getParent();
                ViewGroup container = ((ViewGroup) row.getParent());
                container.removeView(row);
                container.invalidate();
            }
        });
    }
}

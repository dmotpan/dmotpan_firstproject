package com.example.dmotpan.firstproject;

import android.widget.ImageView;

/**
 * Created by dmotpan on 7/5/16.
 */
public class Person {
    public String name;
    public ImageView photo;
    byte [] byteImage;

    public byte[] getByteImage() {
        return byteImage;
    }

    public ImageView getPhoto() {
        return photo;
    }

    public String getName() {
        return name;
    }

    public void setPhoto(ImageView photo) {
        this.photo = photo;
    }

    public void setName(String name) {
        this.name= name;
    }

    public Person(String name, byte [] image) {
        this.name = name;
        this.byteImage = image;
    }
    public Person(String name, ImageView photo) {
        this.name = name;
        this.photo = photo;
    }
}

package com.example.dmotpan.firstproject.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.R;

import java.util.List;

/**
 * Created by dmotpan on 7/5/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private List<Person> list;
    Person person;

    public RecyclerAdapter(List <Person> list) {
        this.list = list;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        person = list.get(position);
        holder.name.setText(person.getName());
        holder.photo.setImageResource(setPhoto());

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addItem(Person person) {
        list.add(person);
        notifyDataSetChanged();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView photo;
        Button button;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            button = (Button) itemView.findViewById(R.id.deleteButton);
            name = (TextView) itemView.findViewById(R.id.firstLastNameTextView);
            photo = (ImageView) itemView.findViewById(R.id.imageViewPhoto);
        }
    }
    public int setPhoto() {
        switch (person.getName()) {
            case Names.ARCADII:
                return R.drawable.arubailo;
            case Names.DMITRII:
                return R.drawable.motpan;
            case Names.FILIP:
                return R.drawable.filip;
            case Names.VADIM:
                return R.drawable.vadimm;
            case Names.SASHA_BULAT:
                return R.drawable.sashab;
            case Names.SASHA_BOND:
                return R.drawable.sashabog;
            default:
                return R.drawable.motpan;
        }
    }

}
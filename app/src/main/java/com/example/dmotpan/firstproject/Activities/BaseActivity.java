package com.example.dmotpan.firstproject.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dmotpan.firstproject.DatabaseHelper;
import com.example.dmotpan.firstproject.Names;
import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.adapters.PersonAdapter;
import com.example.dmotpan.firstproject.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by dmotpan on 7/6/16.
 */
public class BaseActivity extends AppCompatActivity {

    protected ArrayList<Person> list = new ArrayList<Person>();
    protected ListView listView;
    protected PersonAdapter adapter;
    protected ImageView imageView;
    public DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.add_item, menu);
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageView = (ImageView) findViewById(R.id.imageViewPhoto);
        setTitle(getClass().getSimpleName());
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.close();
    }

    public void addPersonToAdapter() {
        adapter.addItem(new Person(Names.randomChoosePerson(), imageView));
        adapter.notifyDataSetChanged();
    }

    public void addFromDatabase () {
        int i = new Random().nextInt(6) + 1;
        Log.e("i = ", String.valueOf(i));
        Person person = databaseHelper.getPerson(i);
        TextView textView = new TextView(this);
        textView.setText(person.getName());
        Bitmap bitmap = BitmapFactory
                .decodeByteArray(person.getByteImage(), 0, person.getByteImage().length);
        imageView = new ImageView(this);
        imageView.setImageBitmap(bitmap);
        imageView.requestLayout();
        person.setPhoto(imageView);

        adapter.addItem(person);
        adapter.notifyDataSetChanged();
    }
}
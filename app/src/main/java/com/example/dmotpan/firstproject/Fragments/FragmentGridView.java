package com.example.dmotpan.firstproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.dmotpan.firstproject.Person;
import com.example.dmotpan.firstproject.adapters.PersonAdapter;
import com.example.dmotpan.firstproject.R;

public class FragmentGridView extends BaseFragment {

    @Override
    public void addPerson(Person person) {
        personAdapter.addItem(person);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_grid_view, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        personAdapter = new PersonAdapter(getActivity());
        gridView.setAdapter(personAdapter);
        return view;
    }

}

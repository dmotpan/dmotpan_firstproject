package com.example.dmotpan.firstproject;

import java.util.Random;

/**
 * Created by dmotpan on 7/6/16.
 */
public class Names {

    public static final String DMITRII = "Dmitrii Motpan";
    public static final String ARCADII = "Arcadii Rubailo";
    public static final String FILIP = "Filip Rosca";
    public static final String SASHA_BULAT = "Alexandr Bulat";
    public static final String VADIM = "Vadim Morozov";
    public static final String SASHA_BOND = "Alexandr Bondarenco";

    public static final String [] NAMES = {DMITRII, ARCADII, FILIP, SASHA_BULAT,
            VADIM, SASHA_BOND};

    public static String randomChoosePerson() {

        Random random = new Random();
        int a = random.nextInt(6);
        return NAMES[a];
    }
}

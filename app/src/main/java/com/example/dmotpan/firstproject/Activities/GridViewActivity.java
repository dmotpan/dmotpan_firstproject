package com.example.dmotpan.firstproject.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.GridView;

import com.example.dmotpan.firstproject.adapters.PersonAdapter;
import com.example.dmotpan.firstproject.R;

public class GridViewActivity extends BaseActivity {

    GridView gridView;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addPerson:
                addPersonToAdapter();
                break;
            case R.id.addPersonDb:
                addFromDatabase();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new PersonAdapter(this);
        gridView.setAdapter(adapter);
    }
}

